require_relative 'items_collection.rb'
require_relative 'discount_collection.rb'

class Basket
  RRP = 8.0

  def initialize(items)
    @items = ItemsCollection.new(items)
  end

  def price
    discount_process = method :calculate_discounts
    1.upto(@items.uniques).map(&discount_process).min || 0
  end

  def pretty_price
    "%2.2f EUR" % price
  end

  private

  def calculate_discounts(discount_size)
    discounts = DiscountCollection.new(@items)
    discounts.calculate_remaining(discount_size)
  end
end
