require_relative "../../basket.rb"

When(/^there are no items in my basket$/) do
  @basket = Basket.new(a: 0, b: 0, c: 0, d: 0, e: 0)
end

Then(/^the price should be '(.+)'$/) do |given_price|
  expect( @basket.pretty_price ).to eql given_price
end

When(/^there are items in my basket$/) do
  @basket = Basket.new(a: 1, b: 1, c: 1, d: 1, e: 1)
end

Then(/^the price should not be '(.+)'$/) do |given_price|
  expect( @basket.pretty_price ).not_to eql given_price
end
