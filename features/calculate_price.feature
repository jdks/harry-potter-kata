Feature: Calculate Price
  As a Harry Potter fan
  I want to calculate the cost of my basket
  So I can decide whether or not to continue with my purchase

  Scenario: Empty basket
    When there are no items in my basket
    Then the price should be '0.00 EUR'

  Scenario: Items in the basket
    When there are items in my basket
    Then the price should not be '0.00 EUR'
