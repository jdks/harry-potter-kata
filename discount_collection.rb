require_relative 'discount.rb'
require_relative 'discount_rates.rb'

class DiscountCollection
  RRP = 8.0

  def initialize(items)
    @items = items.clone
    @price = 0
  end

  def calculate_remaining(discount_size)
    return price if discount_size == 0
    apply_discount(discount_size)
    calculate_remaining(@items.uniques)
  end

  private

  def price
    @price
  end

  def apply_discount(discount_size)
    discount = Discount.new(discount_size)
    @items.apply_discount(discount)
    @price += RRP * DiscountRates[discount_size] * discount_size
  end
end
