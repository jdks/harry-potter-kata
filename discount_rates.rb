class DiscountRates
  @rates = [ 1.0, 0.95, 0.90, 0.80, 0.75 ]

  def self.[](discount_size)
    @rates[discount_size - 1]
  end
end
