class ItemsCollection
  def initialize(items)
    @items = items
  end

  def apply_discount(discount)
    sort!
    @items.merge!(discount.to_h) { |_, a, b| a - b }
  end

  def uniques
    @items.select { |_, v| v > 0 }.size
  end

  private

  def sort!
    @items = Hash[keys.zip(ordered_values)]
  end

  def keys
    @items.keys
  end

  def values
    @items.values
  end

  def ordered_values
    values.sort.reverse
  end
end
