class Discount
  MASK = {a: 1, b: 1, c: 1, d: 1, e: 1}

  def initialize(discount_size)
    @data = Hash[MASK.take(discount_size)]
  end

  def to_h
    @data
  end
end
