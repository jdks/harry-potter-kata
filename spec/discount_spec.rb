require_relative '../discount.rb'

describe Discount do

  def only_contain_values(num)
    @discount.to_h.values.uniq == [num]
  end

  it "should apply up to one discount per item" do
    @discount = Discount.new(5)
    expect( only_contain_values(1) ).to be_true
  end

  it "should only apply to the number of books given" do
    discount = Discount.new(3)
    expect( discount.to_h ).to have(3).items
  end
end
