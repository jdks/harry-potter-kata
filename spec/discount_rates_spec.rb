require_relative '../discount_rates.rb'

describe DiscountRates do
  it "decreases each item price for more items" do
    2.upto(5) do |items|
      expect( DiscountRates[items] ).to be < DiscountRates[items - 1]
    end
  end
end
