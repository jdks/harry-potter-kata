require_relative '../basket.rb'

describe Basket do
  def create_diff_books(num)
    Hash[%I/a b c d e/.zip([1] * num + [0] * (5 - num))]
  end

  def initialize_items
    @items = { a:0, b:0, c:0, d:0, e:0 }
  end

  def update_basket(num_books)
    books = create_diff_books(num_books)
    @items.merge!(books) { |k,a,b| a + b }
  end

  describe "#price" do
    let(:basket) { Basket.new(@items) }
    let(:price) { basket.price }

    before(:each) do
      initialize_items
    end

    context "1 of each book" do
      let(:books) { create_diff_books(@num_books) }

      it "returns correct price for 5 books" do
        update_basket(5)
        expect( price ).to eql 30.0
      end

      it "returns the correct price for 4 books" do
        update_basket(4)
        expect( price ).to eql 25.6
      end

      it "returns the correct price for 3 books" do
        update_basket(3)
        expect( price ).to eql 21.6
      end

      it "returns the correct price for 2 books" do
        update_basket(2)
        expect( price ).to eql 15.2
      end
    end

    context "only 1 book" do
      it "returns the price without any discount" do
        update_basket(1)
        expect( price ).to eql 8.0
      end
    end

    context "one copy of a certain book" do
      it "returns the correct price for 4 books" do
        update_basket(3)
        update_basket(1)
        expect( price ).to eql 29.6
      end

      it "returns the correct price for 6 books" do
        update_basket(5)
        update_basket(1)
        expect( price ).to eql 38.0
      end
    end

    context "two copies of different books" do
      it "returns the correct price for 6 books" do
        update_basket(4)
        update_basket(2)
        expect( price ).to eql 40.80
      end
    end

    context "three copies of different books" do
      it "returns the correct price for 8 books" do
        update_basket(5)
        update_basket(3)
        expect( price ).to eql 51.20
      end
    end
  end
end
