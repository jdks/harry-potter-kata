require_relative '../items_collection.rb'
require_relative '../discount.rb'

describe ItemsCollection do
  describe "#apply_discount" do
    it "returns a collection with less items" do
      items = ItemsCollection.new(a: 1, b: 1, c: 1, d: 1, e: 1)
      discount = Discount.new(2)
      items.apply_discount(discount)
      expect( items.uniques ).to eql 3
    end

    it "applies the discount to the correct items" do
      items = ItemsCollection.new(a: 1, b: 1, c: 0, d: 0, e: 2)
      discount = Discount.new(2)
      items.apply_discount(discount)
      items.apply_discount(discount)
      expect( items.uniques ).to eql 0
    end
  end

  describe "#uniques" do
    it "returns the number of unique items" do
      items = ItemsCollection.new(a: 2, b: 3, c: 1, d: 1, e: 1)
      expect( items.uniques ).to eql 5
    end
  end
end
